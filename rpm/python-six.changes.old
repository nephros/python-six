-------------------------------------------------------------------
Thu Sep 24 19:53:53 CEST 2020 - sailfish@nephros.org

- update to 1.15.0:
  - bump version
  - try to build through gitlab-ci

-------------------------------------------------------------------
Thu Apr 24 11:20:32 UTC 2014 - dmueller@suse.com

- update to 1.6.1:
  - Raise an AttributeError for six.moves.X when X is a module not available in
    the current interpreter.
  - Raise an AttributeError for every attribute of unimportable modules.
  - Issue #56: Make the fake modules six.moves puts into sys.modules appear not to
    have a __path__ unless they are loaded.
  - Pull request #28: Add support for SplitResult.
  - Issue #55: Add move mapping for xmlrpc.server.
  - Pull request #29: Add move for urllib.parse.splitquery.

-------------------------------------------------------------------
Wed Feb 26 16:44:10 UTC 2014 - rschweikert@suse.com

- Include in SLE 12 (FATE #315990)

-------------------------------------------------------------------
Sun Feb  9 17:06:59 UTC 2014 - toms@opensuse.org

- update to 1.5.2:
  - Issue #53: Make the fake modules six.moves puts into sys.modules 
    appear not to have a __name__ unless they are loaded.

Changes of older releases, see CHANGES files in the Bitbucket repo at
https://bitbucket.org/gutworth/six/src/758cacd651f3ee8c9eb2253ca3905a1d46f88786/CHANGES?at=default

-------------------------------------------------------------------
Tue Jan  7 14:52:36 UTC 2014 - speilicke@suse.com

- Bring back argparse requirement for SP3

-------------------------------------------------------------------
Mon Sep  2 15:34:12 UTC 2013 - dmueller@suse.com

- update to 1.4.1:
  - Issue #31: Add six.moves mapping for UserString.
  - Pull request #12: Add six.add_metaclass, a decorator for adding a metaclass to
    a class.
  - Add six.moves.zip_longest and six.moves.filterfalse, which correspond
    respectively to itertools.izip_longest and itertools.ifilterfalse on Python 2
    and itertools.zip_longest and itertools.filterfalse on Python 3.
  - Issue #25: Add the unichr function, which returns a string for a Unicode
    codepoint.
  - Issue #26: Add byte2int function, which complements int2byte.
  - Issue #23: Allow multiple base classes to be passed to with_metaclass.
  - Issue #24: Add six.moves.range alias. This exactly the same as the current
  xrange alias.
  - Pull request #5: Create six.moves.urllib, which contains abstractions for a
    bunch of things which are in urllib in Python 3 and spread out across urllib,
    urllib2, and urlparse in Python 2.

-------------------------------------------------------------------
Tue Jul 30 07:41:52 UTC 2013 - speilicke@suse.com

- Run testsuite
- Build and package HTML documentation

-------------------------------------------------------------------
Mon Apr 29 11:14:05 UTC 2013 - dmueller@suse.com

- update to 1.3.0:
  - In six.iter(items/keys/values/lists), passed keyword arguments through to the
  underlying method.
  - Add six.iterlists().
  - Fix Jython detection.

-------------------------------------------------------------------
Thu Nov 22 14:18:55 UTC 2012 - toddrme2178@gmail.com

- Removed openSUSE 11.4 spec file workarounds

-------------------------------------------------------------------
Wed Sep 12 03:01:59 UTC 2012 - os-dev@jacraig.com

- Update to 1.2.0:
  * Issue #13: Make iterkeys/itervalues/iteritems return iterators on Python 3
    instead of iterables.
  * Issue #11: Fix maxsize support on Jython.
  * Add six.next() as an alias for six.advance_iterator().
  * Use the builtin next() function for advance_iterator() where is available
    (2.6+), not just Python 3.
  * Add the Iterator class for writing portable iterators.

-------------------------------------------------------------------
Tue Jun  5 11:30:58 UTC 2012 - toddrme2178@gmail.com

- Initial spec file


