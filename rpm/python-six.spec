#
# spec file for package python-six for Mer, based on OpenSuse version
#

Name:           python-six
Version:        1.16.0
Release:        0
Url:            http://pypi.python.org/pypi/six/
Summary:        Python 2 and 3 compatibility utilities
License:        MIT
Group:          Development/Libraries
Source:         six-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  python-devel
BuildArch:      noarch
Requires:       python >= 2.6

%description
Six is a Python 2 and 3 compatibility library. It provides utility 
functions for smoothing over the differences between the Python 
versions with the goal of writing Python code that is compatible on 
both Python versions. See the documentation for more information on 
what is provided.

%prep
%setup -q -n six-%{version}

%build
python setup.py build

%install
python setup.py install --prefix=%{_prefix} --root=%{buildroot}

%files
%defattr(-,root,root)
%license LICENSE
%doc LICENSE README.rst
%{python_sitelib}/six.py*
%{python_sitelib}/six-%{version}-py*.egg-info

%changelog

